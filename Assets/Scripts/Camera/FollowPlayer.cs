﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public float trackSpeed = 10f;
	private Transform target;

	void Start() {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	void Update () {
		Vector3 targetPos = new Vector3 (target.position.x, target.position.y, transform.position.z);
		//transform.position = Vector3.MoveTowards (transform.position, targetPos, trackSpeed * Time.deltaTime);
		transform.position = targetPos;
	}
}