﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public GameObject mainPanel;
	public GameObject settingsPanel;
	public GameObject creditsPanel;

	public void MainPanel() {
		mainPanel.SetActive (true);
		settingsPanel.SetActive (false);
		creditsPanel.SetActive (false);
	}

	public void SettingsPanel() {
		mainPanel.SetActive (false);
		settingsPanel.SetActive (true);
	}

	public void CreditsPanel() {
		mainPanel.SetActive (false);
		creditsPanel.SetActive (true);
	}

	public void StageSelect() {
		Application.LoadLevel (Scenes.StageSelect);
	}
}