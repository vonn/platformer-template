﻿using UnityEngine;
using System.Collections;

public class Stage {

	public string stageName;	//corresponds to scene name
	public string bestTime;
	public string bestScore;
	public SoundPlayback levelMIDI;
	public bool available;	//if you are allowed to play this stage
	public bool completed;	//if you have completed this stage

	public Stage(string name) {
		stageName = name;
		available = true;
		PlayerPrefs.SetString (name, "312.2:62:440,440,440,440:.5,.5,.5,.5");
	}

	public void ValuesFromString(string values) {
		if (values.Length > 0)
			completed = true;
		string[] vals = values.Split (':');
		bestTime = GameController.StringifyTime(float.Parse(vals [0]));
		bestScore = vals [1];

		//this does not work
		//needs a new system where the SoundPlayback object is attached to the gamemanager
		//this class can instead contain a SoundEvent representing the level SoundPlayback
		string[] fS = vals [2].Split (',');
		double[] f = new double[fS.Length];
		for (int i = 0; i < f.Length; i++)
			f[i] = float.Parse(fS[i]);
		string[] rS = vals [3].Split (',');
		float[] r = new float[rS.Length];
		for (int i = 0; i < r.Length; i++)
			r[i] = float.Parse(rS[i]);
		SoundEvent thisMIDI = new SoundEvent();
		thisMIDI.frequencies = f;
		thisMIDI.rhythm = r;
		levelMIDI = new SoundPlayback ();
		levelMIDI.AddSoundDontPlay (thisMIDI);
	}
}