﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	
	public string[] stageNames;
	private Dictionary<string, Stage> stagesDict;
	private string selectedStage;

	public GameObject stagePanel;		//for viewing stage info and interacting with it
	public GameObject unavailablePanel;	//for when you try to go to an unavailable stage
	public UILabel timeDisplay;
	public UILabel scoreDisplay;

	void Start () {
		stagesDict = new Dictionary<string, Stage>();
		foreach (Stage s in LoadStages()) {
			s.ValuesFromString(PlayerPrefs.GetString(s.stageName,""));
			stagesDict.Add(s.stageName, s);
		}
	}

	Stage[] LoadStages() {
		// could use stages field
		// or could load stage names from playerprefs
		// either way, names must match scene names
		Stage[] s = new Stage[stageNames.Length];
		for (int i = 0; i < s.Length; i++) {
			s[i] = new Stage(stageNames[i]);
		}
		return s;
	}

	public void SelectStage(string name) {
		Stage stage = stagesDict [name];
		if (stage.available) {
			stagePanel.SetActive(true);
			unavailablePanel.SetActive(false);
			selectedStage = name;
			if (stage.completed) {
				timeDisplay.text = stage.bestTime;
				scoreDisplay.text = stage.bestScore;
			}
		}
		else {
			stagePanel.SetActive(false);
			unavailablePanel.SetActive(true);
			selectedStage = "";
		}
	}

	public void PlayMIDI() {
		if (stagesDict [selectedStage].completed)
			stagesDict [selectedStage].levelMIDI.Play ();
	}
	
	public void PlayLevel() {
		if (stagesDict[selectedStage].available)
			Application.LoadLevel(selectedStage);
	}
}