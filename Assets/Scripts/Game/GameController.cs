﻿using UnityEngine;
using System.Collections;

public enum GameStates {
	Paused,
	Playing
}

public class GameController : MonoBehaviour {
	public GameStates currentState;
	private PlayerController player;
	private int lives = 3;
	private int points;
	private float time;
	#region GUI elements
	public GameObject livePanel;
	public GameObject pausePanel;
	public GameObject deadPanel;
	public UILabel livesDisplay;
	public UILabel pointsDisplay;
	public UILabel timeDisplay;
	public GameObject checkpointDisplay;
	#endregion

	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
		//set initial gamestate
		currentState = GameStates.Playing;
	}
	
	void Update () {
		if (currentState == GameStates.Playing) {
			time += Time.deltaTime;
		}
		livesDisplay.text = "x" + lives;
		pointsDisplay.text = "x" + points;
		timeDisplay.text = StringifyTime(time);
	}

	public void PausePlayGame() {
		if (currentState == GameStates.Playing) {
			currentState = GameStates.Paused;
			Time.timeScale = 0;
			pausePanel.SetActive(true);
		}
		else {
			currentState = GameStates.Playing;
			Time.timeScale = 1;
			pausePanel.SetActive(false);
		}
	}

	public void AddPoint() {
		points++;
	}

	public void AddLife() {
		lives++;
	}

	public void CheckpointReached() {
		// TODO remember game state (points, time, enemies, etc.)
		//GUI alert
		checkpointDisplay.SetActive (true);
		TweenAlpha cpTween = checkpointDisplay.GetComponent<TweenAlpha> ();
		cpTween.PlayForward ();
	}

	public void GameOver() {
		currentState = GameStates.Paused;
		livePanel.SetActive (false);
		deadPanel.SetActive (true);
		lives--;
	}

	public void Reset() {
		if (lives < 0)
			return;
		// TODO restore game state (points, time, enemies, etc.)
		currentState = GameStates.Playing;
		livePanel.SetActive (true);
		deadPanel.SetActive (false);
		player.Respawn ();
	}

	#region GUI-related methods
	public void ReverseCheckpointDisplay() {
		TweenAlpha cpTween = checkpointDisplay.GetComponent<TweenAlpha> ();
		cpTween.PlayReverse ();
	}

	public static string StringifyTime(float s) {
		float hours;
		float minutes;
		float seconds;
		seconds = Mathf.Floor (s % 60);
		minutes = Mathf.Floor(s / 60);
		hours = Mathf.Floor(s / 360);
		return (hours < 10 ? "0" : "") + hours + (minutes < 10 ? ":0" : ":") + minutes + (seconds < 10 ? ":0" : ":") + seconds;
	}
	#endregion
}