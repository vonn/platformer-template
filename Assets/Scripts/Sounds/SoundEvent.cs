﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; //for Dictionary

/// <summary>
/// Contains musical info to be played back
/// </summary>
public class SoundEvent : MonoBehaviour {
	//frequencies and rhythm must be the same length
	public string[] notes;
	public double[] frequencies;
	public float[] rhythm;
	public bool useNotes;
}