﻿using UnityEngine;
using System; //for Math
using System.Collections;
using System.Collections.Generic; //for List

/// <summary>
/// Collects SoundEvent objects and plays them back as one song
/// </summary>
public class SoundPlayback : MonoBehaviour {

	private List<SoundEvent> sounds;
	private List<double> frequencies;
	private List<float> rhythm;

	private float nextNoteTime = 0;
	private int step = 0;
	private double frequency;
	private bool playing;

	public double gain = .5;
	public float bpm = 60;	//beats per minute
	public bool loop;	//not yet implemented

	private double increment;
	private double phase;
	private double sampling_frequency = 48000;

	public static Dictionary<string, double> noteLibrary = new Dictionary<string, double>();

	void Awake() {
		noteLibrary.Add ("C4", 261.63);
		noteLibrary.Add ("C#4", 277.18);
		noteLibrary.Add ("Db4", 277.18);
		noteLibrary.Add ("D4", 293.66);
		noteLibrary.Add ("D#4", 311.13);
		noteLibrary.Add ("Eb4", 311.13);
		noteLibrary.Add ("E4", 329.63);
		noteLibrary.Add ("F4", 349.23);
		noteLibrary.Add ("F#4", 369.99);
		noteLibrary.Add ("Gb4", 369.99);
		noteLibrary.Add ("G4", 392.00);
		noteLibrary.Add ("G#4", 415.30);
		noteLibrary.Add ("Ab4", 415.30);
		noteLibrary.Add ("A4", 440);
		noteLibrary.Add ("A#4", 466.16);
		noteLibrary.Add ("Bb4", 466.16);
		noteLibrary.Add ("B4", 493.88);
		noteLibrary.Add ("C5", 523.25);
		noteLibrary.Add ("C#5", 554.37);
		noteLibrary.Add ("Db5", 554.37);
		noteLibrary.Add ("D5", 587.33);
		noteLibrary.Add ("D#5", 622.25);
		noteLibrary.Add ("Eb5", 622.25);
		noteLibrary.Add ("E5", 659.25);
		noteLibrary.Add ("F5", 698.46);
		noteLibrary.Add ("F#5", 739.99);
		noteLibrary.Add ("Gb5", 739.99);
		noteLibrary.Add ("G5", 783.99);
		noteLibrary.Add ("G#5", 830.61);
		noteLibrary.Add ("Ab5", 830.61);
		noteLibrary.Add ("A5", 880);
		noteLibrary.Add ("A#5", 932.33);
		noteLibrary.Add ("B5", 987.77);
		noteLibrary.Add ("C6", 1046.50);
	}

	void Start () {
		playing = false;
		sounds = new List<SoundEvent> ();
	}
	
	void Update () {
		if (!playing)
			return;
		if (Time.time >= nextNoteTime) {
			if (step >= rhythm.Count) {
				step = 0;
				playing = loop;	//stop if not loop
				return;
			}
			nextNoteTime = Time.time + rhythm[step];
			frequency = frequencies[step];
			step++;
		}
	}

	void OnAudioFilterRead(float[] data, int channels) {
		if (!playing)
			return;
		// update increment in case frequency has changed
		increment = frequency * 2 * Math.PI / sampling_frequency;
		for (var i = 0; i < data.Length; i = i + channels) {
			phase = phase + increment;
			// this is where we copy audio data to make them available to Unity
			data[i] = (float)(gain*Math.Sin(phase));
			// if we have stereo, we copy the mono data to each channel
			if (channels == 2) data[i + 1] = data[i];
			if (phase > 2 * Math.PI) phase = 0;
		}
	}

	/// <summary>
	/// Constructs a fluid playback from the sounds acquired so far
	/// </summary>
	void Construct() {
		frequencies = new List<double> ();
		rhythm = new List<float> ();
		for (int s = 0; s < sounds.Count; s++) {
			for (int i = 0; i < sounds[s].frequencies.Length; i++) {
				frequencies.Add(sounds[s].frequencies[i]);
				rhythm.Add (sounds[s].rhythm[i] * (60 / bpm));
			}
		}
	}

	/// <summary>
	/// Constructs a temporary playback for one SoundEvent and plays it
	/// </summary>
	/// <param name="s">Sound Event to play</param>
	void ConstructAndPlayOne(SoundEvent s) {
		frequencies = new List<double> ();
		rhythm = new List<float> ();
		for (int i = 0; i < s.frequencies.Length; i++) {
			frequencies.Add(s.frequencies[i]);
			rhythm.Add (s.rhythm[i] * (60 / bpm));
		}
		playing = true;
		loop = false;
		step = 0;
	}

	/// <summary>
	/// Converts array of musical notes as strings and converts them to array of frequencies
	/// </summary>
	/// <returns>Array of frequencies (doubles)</returns>
	/// <param name="notes">Notes to convert</param>
	double[] NotesToFrequencies(string[] notes) {
		double[] freqs = new double[notes.Length];
		for (int i = 0; i < freqs.Length; i++) {
			freqs[i] = noteLibrary[notes[i]];
		}
		return freqs;
	}

	/// <summary>
	/// Adds a sound to the sequence of this playback
	/// Plays that sound
	/// </summary>
	/// <param name="s">Sound Event to be added</param>
	public void AddSound(SoundEvent s) {
		if (s.useNotes)
			s.frequencies = NotesToFrequencies(s.notes);
		sounds.Add (s);
		ConstructAndPlayOne (s);
	}

	/// <summary>
	/// Adds a sound to the sequence of this playback
	/// Does not play that sound
	/// </summary>
	/// <param name="s">Sound Event to be added</param>
	public void AddSoundDontPlay(SoundEvent s) {
		if (sounds == null)	//because this is likely called from the stages menu
			sounds = new List<SoundEvent>();
		if (s.useNotes)
			s.frequencies = NotesToFrequencies(s.notes);
		sounds.Add (s);
	}

	/// <summary>
	/// Constructs the full playback and plays it from the start
	/// </summary>
	public void Play() {
		Construct ();
		step = 0;
		playing = true;
	}

	public void PausePlay() {
		playing = !playing;
	}

	public void Rewind() {
		step = 0;
	}

	/// <summary>
	/// Get all frequencies from this playback
	/// </summary>
	/// <returns>The frequencies.</returns>
	public double[] AllFrequencies() {
		List<double> f = new List<double>();
		for (int s = 0; s < sounds.Count; s++) {
			for (int i = 0; i < sounds[s].frequencies.Length; i++) {
				f.Add(sounds[s].frequencies[i]);
			}
		}
		return f.ToArray ();
	}

	/// <summary>
	/// Get the rhythm from this playback
	/// </summary>
	/// <returns>The rhythm (not affected by bpm)</returns>
	public float[] AllRhythm() {
		List<float> r = new List<float>();
		for (int s = 0; s < sounds.Count; s++) {
			for (int i = 0; i < sounds[s].frequencies.Length; i++) {
				r.Add(sounds[s].rhythm[i]);
			}
		}
		return r.ToArray ();
	}
}