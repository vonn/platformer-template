﻿using UnityEngine;
using System;  // Needed for Math

public class Sinus : MonoBehaviour
{
	float noteTime;
	// un-optimized version
	public double[] frequenciesAmNatural = {440,493.88,525.25,587.33,659.25,698.46,783.99,880};
	public double[] frequenciesAmPentatonic = {440,525.25,587.33,659.25,783.99,880};
	public double[] frequenciesSomeDeal = {440,525.25,659.25,783.99,880,783.99,525.25};
	public double[] frequencies;
	public float[] rhythm = {.2f,.2f,.2f,.2f,.2f,.2f,.2f,.2f,.1f,.1f,.2f,.2f,.2f,.2f,.2f,.2f,.2f};
	public int rhythmStep;
	public double frequency;
	public double gain = 0.5;
	
	private double increment;
	private double phase;
	private double sampling_frequency = 48000;

	void Start() {
		rhythmStep = 0;
		noteTime = 0;
		frequencies = frequenciesSomeDeal;
	}

	void FixedUpdate() {
		if (Time.time > noteTime) {
			noteTime += rhythm[rhythmStep];
			rhythmStep = rhythmStep == rhythm.Length - 1 ? 0 : rhythmStep + 1;
			double newFrequency = frequency;
			while (newFrequency == frequency) {
				newFrequency = frequencies[UnityEngine.Random.Range(0,frequencies.Length)];
			}
			frequency = newFrequency;
		}
	}

	void OnAudioFilterRead(float[] data, int channels)
	{
		// update increment in case frequency has changed
		increment = frequency * 2 * Math.PI / sampling_frequency;
		for (var i = 0; i < data.Length; i = i + channels)
		{
			phase = phase + increment;
			// this is where we copy audio data to make them “available” to Unity
			data[i] = (float)(gain*Math.Sin(phase));
			// if we have stereo, we copy the mono data to each channel
			if (channels == 2) data[i + 1] = data[i];
			if (phase > 2 * Math.PI) phase = 0;
		}
	}
} 