﻿using UnityEngine;
using System.Collections;

public class PatrolEnemy : Enemy {

	public float thinkCooldown;
	public float speed = .1f;
	public float sightRange = 3.5f;
	public bool canSeePlayer;

	void Start () {
		// setup base class variables
		thinkCD = thinkCooldown;
	}

	protected override void Update() {
		base.Update ();

		if (!canSeePlayer)
			canSeePlayer = LookForPlayer ();
		//chase after player if canSeePlayer
	}

	protected override void Think() {
		// tell the base class that we thought
		base.Think ();

		if (!props.grounded) return;

		// simple left-right patrolling;
		if (canSeePlayer) {
			speed *= 1.5f;
			sprite.color = Color.red;
		}
		else {
			speed = -speed;
		}

		MoveX(speed);

		props.vel.y = 0;
	}

	bool LookForPlayer() {
		//cast another ray as well at mid height
		Vector2 origin = new Vector2(transform.position.x, transform.position.y + /*height*/.5f);
		Vector2 dir = new Vector2(speed,0).normalized;
		RaycastHit2D hit = Physics2D.Raycast (origin, dir, sightRange, Layers2D.defaultLayerMask);
		// false if there was no hit or hit was not player
		if (hit && hit.transform.tag == Tags.Player) {
			//Debug.Log(hit.transform.tag);
			Debug.DrawRay (origin, dir*sightRange, Color.red);
			return true;
		}
		else {
			Debug.DrawRay (origin, dir*sightRange, Color.green);
			return false;
		}
		/*// true if player is not hiding or is already discovered
		PlayerController pc = hit.transform.gameObject.GetComponent<PlayerController>();
		//is here where player should be set to discovered?*/
	}
	/*
	void FixedUpdate() {
		base.FixedUpdate();
	}
	*/
}