﻿using UnityEngine;
using System.Collections;

public class Enemy : Body2D {

	protected SpriteRenderer sprite;
	protected float thinkCD;
	float thinkTime;

	protected override void Awake() {
		base.Awake();
		sprite = transform.FindChild("Sprite").GetComponent<SpriteRenderer>();
		//print ("enemy awake");
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	protected override void Update () {
		base.Update ();

		// is it time to think?
		if (Time.time > thinkTime && _gc.currentState != GameStates.Paused) {
			Think ();
		}
	}

	protected override void FixedUpdate() {
		base.FixedUpdate();
	}

	/// <summary>
	/// What the AI thinks about when he thinks
	/// </summary>
	protected virtual void Think() {
		// Do the thing
		//print ("I think");

		// update think time
		thinkTime = Time.time + thinkCD;
	}
}