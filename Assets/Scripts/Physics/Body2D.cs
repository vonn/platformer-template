﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// An abstraction of an object with a physical body in our game world.
/// </summary>
public abstract class Body2D : MonoBehaviour {
	#region Member Variables
	/// <summary>
	/// The physical properties of a body.
	/// </summary>
	[SerializeField]
	protected PhysicalProperties2D props;
	protected Transform _trans;
	protected Rigidbody2D _rb2d;
	protected GameController _gc;
	protected BoxCollider2D _col;
	bool groundNextFrame;
	Vector2 groundNextFramePos;
	Vector2 hitLeftPos = Constants.crudVector;
	Vector2 hitRightPos = Constants.crudVector;
	Vector2 wallNextFramePos;
	Vector2 ceilNextFramePos = Constants.crudVector;
	[HideInInspector]
	public Transform originalParent;
	[SerializeField]
	MovingPlatform currentMovingPlatform;
	float canLandOnOneWayCD = .5f;
	float canLandOnOneWayTime;
	#endregion

	protected virtual void Awake () {
		// set up var shortcuts
		_trans = transform;
		_col = GetComponent<BoxCollider2D>();
		_rb2d = rigidbody2D ?? null;
		_gc = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>();

		// set up initial values for properties
		//props.mass = 1.0f;
		//props.maxVel = new Vector2(.2f, 1);
		//props.accel = new Vector2(.05f, 0);
		originalParent = transform.parent;
	}
	#region Updates
	protected virtual void Update() {
	}

	/// <summary>
	/// Updates the game at a fixed interval. Physics calculations happen here.
	/// </summary>
	protected virtual void FixedUpdate() {
		if (_gc.currentState == GameStates.Playing) {
			// apply gravity
			if (!props.grounded && !props.ignoreGravity) {

				props.vel.y += World2D.gravity.y;

				// make sure i don't go past terminal velocity
				if (Mathf.Abs(props.vel.y) > props.maxVel.y) {
					props.vel.y = Mathf.Sign(props.vel.y) * props.maxVel.y;
				}
			}
			else if (props.grounded) {
				props.vel.y = 0;
			}

			// == UPDATE NEXT FRAME POSITIONS ==
			// ground
			if (groundNextFrame) {
				//print ("snapping to ground");
				Vector2 pos = new Vector2();
				pos.x = _trans.position.x;
				//pos.y = _col.bounds.min.y;
				pos.y = groundNextFramePos.y;
				//_trans.position = new Vector2(_trans.position.x + props.vel.x, groundNextFramePos.y);
				_trans.position = pos;
				groundNextFrame = false;
				props.grounded = true;

				return;
			}
			// ceiling
			if (ceilNextFramePos != Constants.crudVector) {
				float height = _col.size.y;
				Vector2 newPos = new Vector2();
				newPos.y = ceilNextFramePos.y - height;
				newPos.x = ceilNextFramePos.x;
				props.vel.y = 0;
				ceilNextFramePos = Constants.crudVector;
			}
			// horizontal
			if (hitLeftPos != Constants.crudVector || hitRightPos != Constants.crudVector) {
				Vector2 pos = new Vector2();

				float dist = 0;
				if (hitLeftPos != Constants.crudVector) {
					dist = _trans.position.x - hitLeftPos.x; 
					pos.x = hitLeftPos.x + dist;
				}
				else if (hitRightPos != Constants.crudVector) {
					dist = hitRightPos.x - _trans.position.x;
					pos.x = hitRightPos.x - dist;
				}

				pos.y = _trans.position.y;

				//print ("horizontal snap");

				_trans.position = pos;
				props.hitInFront = true;
				props.vel.x = 0; // to prevent tunnel this frame
				// get rid of these
				hitLeftPos = Constants.crudVector;
				hitRightPos = Constants.crudVector;
			}
			else {
				props.hitInFront = false;
			}

			if (Mathf.Abs(props.vel.y) < Constants.epsilon && !props.ignoreGravity) {
				//GroundType gnd = GroundCheckRay();
				props.currentGroundType = GroundCheckRay();

				switch (props.currentGroundType) {
				case GroundType.Moving:
				case GroundType.OneWay:
				case GroundType.Normal:
					props.grounded = true;
					break;
				case GroundType.NONE:
					props.grounded = false;
					if (currentMovingPlatform) LeaveMovingPlatform();
					break;
				default:
					break;
				}
			}

			_trans.position += (Vector3)props.vel;

			// Check ahead for ground to prevent tunneling
			if (props.vel.y < 0 && !props.ignoreGravity) {
				
				if (currentMovingPlatform) LeaveMovingPlatform();
				
				groundNextFrame = CheckVerticalNextFrame(-1);
			}
			// and ceiling
			else if (props.vel.y > 0) {
				CheckVerticalNextFrame(1);
			}

			CheckHorizontalNextFrame();
		}
	}
	
	protected virtual void LateUpdate() {

	}
	#endregion

	#region Movement
	protected virtual void MoveX(float speed) {
		// shut it up if we're at a wall
		int sideHit = WallCheckRay();
		if ((speed < 0 && sideHit == -1) || 
		    (speed > 0 && sideHit == 1)) {

			speed = 0;
			props.hitInFront = true;
		}

		float addedSpeed = 0;
		bool moving = true;
		//add friction
		if (speed == 0) {
			moving = false;
		}
		/*//for when turning around
		if (speed > 0 != props.vel.x > 0 && props.vel.x != 0) {
			speed *= 10;
		}
		if (props.grounded) {
			addedSpeed = speed;
		}
		else {
			addedSpeed = speed;
		}*/
		//adjust for max speed
		float newSpeed = 0;
		if (moving) {
			newSpeed = props.vel.x + props.accel.x * Mathf.Sign(speed);
		}
		else {	//apply friction if no input
			//eventually player should just stop
			if (Mathf.Abs(newSpeed) < .005f) {
				newSpeed = 0;
			}
		}
		if (Mathf.Abs(newSpeed) <= props.maxVel.x) {

			// nerf speed when switching directions in the air
			if (!props.grounded && 
			    (Mathf.Sign(newSpeed) != Mathf.Sign(props.vel.x))) {
				//print ("spoon");
				newSpeed *= .1f;
			}
			props.vel.x = newSpeed;
		}
	}

	protected void RestoreOriginalParent() {
		transform.parent = originalParent;
	}

	protected void LeaveMovingPlatform() {
		currentMovingPlatform.RemoveRider (this);
		currentMovingPlatform = null;
	}

	protected void FallThroughOneWayPlatform() {
		//print ("foo");
		canLandOnOneWayTime = Time.time + canLandOnOneWayCD;
		// force him down 1 frame of gravity
		_trans.position = new Vector2(_trans.position.x + props.vel.x, 
		                              _trans.position.y + World2D.gravity.y);
		props.grounded = false;
	}
    #endregion

	#region Ray Checking
	/// <summary>
	/// Checks the vertical next frame.
	/// </summary>
	/// <returns><c>true</c>, if vertical next frame was checked, <c>false</c> otherwise.</returns>
	/// <param name="direction">Direction. -1 for down, 1 for up</param>
	bool CheckVerticalNextFrame(int direction) {
		const int numRays = 3;
		float offset = _col.size.x / (numRays - 1);
		// direction down? start at bottom-left, otherwise start at top-left
		Vector2 origin = direction == -1 ? (Vector2)_col.bounds.min : direction == 1 ? 
			new Vector2(_col.bounds.min.x, _col.bounds.max.y) : Vector2.zero;
		//origin.x += offset/2;
		Vector2 dir = props.vel.normalized;
		dir.x = 0;
		float checkDist = props.vel.magnitude;
		bool anyHit = false;

		for (int i = 0; i < numRays; i++) {
			// cast ray
			RaycastHit2D hit = Physics2D.Raycast(origin, dir, checkDist, Layers2D.groundMask);

			if (hit) {
				// hit a ground
				if (direction == -1 && hit.normal.y > 0) {
					// if we hit a 1way plat, see if we can even
					if (hit.transform.GetComponent<OneWayPlatform>() != null &&
					    canLandOnOneWayTime > Time.time) {
						
						Debug.DrawRay(origin, dir*checkDist, Color.black);
					}
					else {					
						Debug.DrawRay(origin, dir*checkDist, Color.red);
						groundNextFramePos = hit.point;
						anyHit = true;
					}
				} 
				// hit a ceiling
				else if (direction == 1 && hit.normal.y < 0) {
					Debug.DrawRay(origin, dir*checkDist, Color.red);
					ceilNextFramePos = hit.point;
					anyHit = true;
					//print("bonk");
				}
			}
			else {
				Debug.DrawRay(origin, dir*checkDist, Color.green);
			}

			origin.x += offset;
		}
		return anyHit;
	}

	bool CheckHorizontalNextFrame() {
		const int numRays = 3;
		float offset = _col.size.y / (numRays/* - 1*/);
		Vector2 origin = _col.bounds.min;
		Vector2 dir = props.vel.normalized;
		dir.y = 0;
		float checkDist = props.vel.magnitude;
		bool anyHit = false;
		origin.y += offset/2;

		for (int i = 0; i < numRays; i++) {
			// Left
			if (props.vel.x < 0) {
				// cast ray
				RaycastHit2D hit = Physics2D.Raycast(origin, dir, checkDist, Layers2D.groundMask);

				if (hit && hit.normal.x > 0) {				
					Debug.DrawRay(origin, dir*checkDist, Color.red);
					// TODO let us know we're gonna hit next frame
					hitLeftPos = hit.point;
					anyHit = true;
				} else {
					Debug.DrawRay(origin, dir*checkDist, Color.green);
				}
			}
			// Right
			else if (props.vel.x > 0) {
				// move the origin's x over
				origin.x = _col.bounds.max.x;
				// cast ray
				RaycastHit2D hit = Physics2D.Raycast(origin, dir, checkDist, Layers2D.groundMask);
				
				if (hit && hit.normal.x < 0) {				
					Debug.DrawRay(origin, dir*checkDist, Color.red);
					// TODO let us know we're gonna hit next frame
					hitRightPos = hit.point;
					anyHit = true;
				} else {
					Debug.DrawRay(origin, dir*checkDist, Color.green);
				}
			}
			
			origin.y += offset;
		}
		
		return anyHit;
	}

	/// <summary>
	/// Walls the check ray.
	/// </summary>
	/// <returns>-1 for left, 0 for none, 1 for right</returns>
	int WallCheckRay() {
		const int numRays = 6;
		float dist = .1f;
		float offset = _col.size.y / (numRays);
		bool anyHit = false;
		Vector2 origin = _col.bounds.min;
		Vector2 dir = -Vector2.right;
		float initOffsetFactor = offset * .2f;
		origin.y += initOffsetFactor;
		int sideHit = 0;
		int currentSide = -1; // start left
		bool onlyBottomHit = false;
		Vector2 fixPos;
		Transform fixTrans = _trans;
		int fixSide = 0;

		for (int i = 0; i < 2; i++ ) {
			for (int ii = 0; ii < numRays; ii++) {
				RaycastHit2D hit = Physics2D.Raycast(origin, dir, dist, Layers2D.groundMask);

				if (hit && hit.normal == -dir) {
					Debug.DrawRay(origin, dir*dist, Color.red);
					anyHit = true;
					sideHit = currentSide;
					// bottom ray only? (HACK)
					if (ii <= Mathf.Floor(numRays/3)) {
						onlyBottomHit = true;
						fixPos = hit.point;
						fixTrans = hit.transform;
						fixSide = currentSide;
					}
					else {
						onlyBottomHit = false;
					}
				}
				else {
					Debug.DrawRay(origin, dir*dist, Color.green);
				}
				origin.y += offset;
			}
			currentSide = 1; // check right now
			dir = Vector2.right;
			origin.x = _col.bounds.max.x;
			origin.y = _col.bounds.min.y + initOffsetFactor;
		}
		// HACK try to kick the body back (should fix corner bug)
		if (onlyBottomHit) {
			//print ("corner bug?");
			Collider2D col = fixTrans.collider2D;
			Vector2 targetCorner;
			Vector2 myCorner;
			Vector2 backup;
			// hit on left
			if (fixSide == -1) {
				//print ("on left");
				// get top right of target
				targetCorner = col.bounds.max;
				// and my bottom left
				myCorner = _col.bounds.min;
			}
			// hit on right
			else {
				//print ("on right");
				// get top left of target
				targetCorner.x = col.bounds.min.x;
				targetCorner.y = col.bounds.max.y;
				// and my bottom-right
				myCorner.x = _col.bounds.max.x;
				myCorner.y = _col.bounds.min.y;
			}
			backup = myCorner - targetCorner;
			// SAT collision
			if (backup.x < backup.y) {
				backup.y = 0;
			}
			else if (backup.y < backup.x) {
				backup.x = 0;
			}
			if (props.grounded)
				_trans.position -= (Vector3)backup * 1.009f;
		}

		//print ("side : " + sideHit);
		return sideHit;
	}

	GroundType GroundCheckRay () {
		float dist = .1f;
		int numRays = 4; // 2 feet
		float offset = _col.size.x / (numRays-1);
		/*Ray2D ray = new Ray2D(
			new Vector2 (_col.bounds.min.x + _col.size.x/2, _col.bounds.min.y),
			-Vector2.up);*/
		RaycastHit2D hit = new RaycastHit2D();
		bool anyHit = false;
		Transform currentGroundTransform = transform;
		bool allHitSameGround = true;

		for (int i = 0; i < numRays; i++) {
			// set up ray
			Vector2 rayOrigin = new Vector2();
			rayOrigin.x = _col.bounds.min.x + (i*offset);
			rayOrigin.y = _col.bounds.min.y;
			Vector2 rayDir = -Vector2.up;
			// cast it
			hit = Physics2D.Raycast(rayOrigin, rayDir, dist, Layers2D.groundMask);
			// hit ground!
			if (hit && hit.normal.y > 0) {
				Debug.DrawRay(rayOrigin, rayDir * dist, Color.red);
				anyHit = true;

				// make sure it's the same ground
				if (currentGroundTransform == transform) {
					currentGroundTransform = hit.transform;
				}
				else {
					allHitSameGround = allHitSameGround && hit.transform == currentGroundTransform;
				}
			}
			else {
				allHitSameGround = false;
				Debug.DrawRay(rayOrigin, rayDir * dist, Color.green);
			}
		}
		// all rays are hitting same ground
		if (anyHit && allHitSameGround) {
			//print ("newground");
			return HandleGround(hit.transform);
		}
		// there's a hit but not all on the same ground
		if (anyHit) {
			//print ("old ground");
			if (props.currentGroundType != GroundType.NONE) {
				return props.currentGroundType;
			}

			return HandleGround(currentGroundTransform);
		}
		//print ("no ground");
		return GroundType.NONE;
	}

	GroundType HandleGround(Transform ground) {
		MovingPlatform mp;
		OneWayPlatform owp;
		// Moving platform!?
		if (ground.parent != null && ground.parent.GetComponent<MovingPlatform>() != null) {
			mp = ground.parent.GetComponent<MovingPlatform>();

			if (!mp.HasRider(this)) {
				mp.AddRider(this);
				currentMovingPlatform = mp;
			}
			return GroundType.Moving;
		}
		// One-way platform?!
		owp = ground.GetComponent<OneWayPlatform>();
		if (owp != null) {
			// verify the landing
			if (owp.ConfirmLand(this)) {
				//print ("good!");
				return GroundType.OneWay;
			}
			//print ("no good");
			return GroundType.NONE;
		}
		// normal ground~
		return GroundType.Normal;
	}
	#endregion

	#region Collisions (kinda obsolete)
	protected virtual void OnTriggerStay2D(Collider2D col) {
		string colTag = col.transform.tag;
		
		// react differently depending on collision
		switch (colTag) {
		case Tags.Terrain:
			if (Mathf.Approximately(_col.bounds.min.y, col.bounds.max.y)){
				props.grounded = true;
			}
			break;
		}
	}

	protected virtual void OnCollisionEnter2D(Collision2D col) {
		string colTag = col.transform.tag;
		ContactPoint2D[] colContacts = col.contacts;

		// react differently depending on collision
		switch (colTag) {
		case Tags.Terrain:
			// check Collision points
			for (int i = 0; i < colContacts.Length; i++) {
				// ground check
				if (props.vel.y < 0 && !props.grounded) {
					// hit the ground
					if (colContacts[i].normal.y > 0) {
						// and if that point is not above the bottom of my collider
						if (colContacts[i].point.y <= _trans.position.y && !props.ignoreGravity) {
							if (col.transform.GetComponent<OneWayPlatform>() != null) {

							}
							else {
								props.grounded = true;
							}
						}
					}
				}
				// hit a ceiling
				else if (colContacts[i].normal.y < 0) {
					//print ("hit ceil");
					props.vel.y = 0;
				}
			}
			break;
		default:
			break;
		}
	}

	protected virtual void OnCollisionStay2D(Collision2D col) {
		string colTag = col.transform.tag;
		ContactPoint2D[] colContacts = col.contacts;
		
		// react differently depending on collision
		switch (colTag) {
		case Tags.Terrain:
			// check Collision points
			for (int i = 0; i < colContacts.Length; i++) {

			}
			break;
		default:
			break;
		}
	}

	protected virtual void OnCollisionExit2D(Collision2D col) {
		string colTag = col.transform.tag;
		ContactPoint2D[] colContacts = col.contacts;
		
		// react differently depending on collision
		switch (colTag) {
		case Tags.Terrain:
			// ground check
			for (int i = 0; i < colContacts.Length; i++) {
				if (colContacts[i].normal.y > 0) {
					props.grounded = false;
				}
			}
			break;
		default:
			break;
		}
	}
	#endregion
}

/// <summary>
/// Physical properties of a 2d object
/// </summary>
[System.Serializable]
public class PhysicalProperties2D {
	public bool ignoreGravity;
	public bool hitInFront;
	public bool grounded;
	public Vector2 vel;
	public Vector2 maxVel;
	public Vector2 accel;
	public GroundType currentGroundType;
	public float mass;
};
