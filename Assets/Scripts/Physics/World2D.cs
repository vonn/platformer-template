﻿using UnityEngine;
using System.Collections;

/// <summary>
/// An abstraction of the properties of our physical game world.
/// </summary>
public class World2D : MonoBehaviour {
	[SerializeField]
	public static Vector2 gravity = new Vector2(0, -.01f);

}
