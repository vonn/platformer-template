﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// A platform you can jump up through
/// </summary>
public class OneWayPlatform : SpecialGround {

	BoxCollider2D _col;

	void Awake () {
		groundType = GroundType.OneWay;
		_col = GetComponent<BoxCollider2D>();
	}

	public bool ConfirmLand(Body2D body) {
		float diff = Mathf.Abs(_col.bounds.max.y - body.collider2D.bounds.min.y);
		//print (diff);
		// make sure the body is above me
		if (diff < Constants.epsilon/5) {
			//print ("confirmed");
			return true;
		}
		//print ("denied");
		return false;
	}
}
