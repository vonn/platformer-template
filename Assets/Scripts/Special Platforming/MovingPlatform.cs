﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// A moving platform moves along its nodes
/// </summary>
public class MovingPlatform : SpecialGround {
	Transform platform;
	[SerializeField]
	Transform riders;
	Transform nodes;
	int currentNode = 0;
	bool isMoving;
	bool backtrack;
	float moveTime;
	float moveCD = 1.0f;
	float moveSpeed = 2f;

	void Awake () {
		groundType = GroundType.Moving;
		platform = transform.FindChild("Platform");
		riders = platform.FindChild("Riders");
		nodes = transform.FindChild("Nodes");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (!isMoving && moveTime < Time.time) {
			// toggle backtracking
			if ((currentNode == 0 && backtrack) || 
			    (currentNode == nodes.childCount-1) && !backtrack ) {

				backtrack = !backtrack;
			}
			// update current node
			if (backtrack) currentNode--;
			else currentNode++;
			// move to next node
			StartCoroutine(MoveToNextNode(nodes.GetChild(currentNode)));
		}
	}

	IEnumerator MoveToNextNode(Transform node) {
		isMoving = true;

		while (Vector2.Distance(platform.position, node.position) > .05f) {
			//platform.position = Vector2.Lerp(platform.position, node.position, moveSpeed * Time.deltaTime);
			Vector3 translation = node.position - platform.position;
			translation = translation.normalized * moveSpeed * Time.deltaTime;
			platform.Translate(translation);

			yield return null;
		}
		// if at last or first node, wait
		if (currentNode == 0 || currentNode == nodes.childCount-1) {
			moveTime = Time.time + moveCD;
		}
		isMoving = false;
	}

	public void AddRider(Body2D body) {
		body.transform.parent = riders;
	}

	public void RemoveRider(Body2D body) {
		for (int i = 0; i < riders.childCount; i ++) {
			Body2D currentBody = riders.GetChild(i).GetComponent<Body2D>();
			if (body == currentBody) {
				currentBody.transform.parent = currentBody.originalParent;
			}
		}
	}

	public bool HasRider(Body2D body) {
		for (int i = 0; i < riders.childCount; i++) {
			if (riders.GetChild(i).GetComponent<Body2D>() == body) {
				return true;
			}
		}
		return false;
	}
}
