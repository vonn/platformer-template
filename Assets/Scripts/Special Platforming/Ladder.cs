﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour {

	// needa know about the player and his input
	public PlayerController player;
	public PlayerInputController input;

	// Use this for initialization
	void Start () {
		// we wanna know about that player
		player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
		input = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerInputController>();
	}

	void OnTriggerEnter2D(Collider2D col) {
		//print ("foo");
		// if we're hitting the player and he presses up
		if (col.tag == Tags.Player &&
		    Input.GetAxis("Vertical") > Constants.epsilon &&
		    !player.climbing) {

			player.ClimbOn();
		}
	}

	void OnTriggerStay2D(Collider2D col) {
		//print ("barring");
		// if we're hitting the player and he presses up
		if (col.tag == Tags.Player &&
		    Mathf.Abs(Input.GetAxis("Vertical")) > Constants.epsilon &&
		    !player.climbing) {

			player.ClimbOn();
		}
	}

	void OnTriggerExit2D(Collider2D col) {
		//print ("snafu");
		if (col.tag == Tags.Player && player.climbing) {
			BoxCollider2D ladderCol = GetComponent<BoxCollider2D>();
			// did the player exit at top of ladder?
			bool topLadder = col.bounds.min.y >= ladderCol.bounds.max.y;
			if (topLadder) {
				player.ClimbZenith();
			}
			else {
				player.ClimbOff();
			}
		}
		//player = null;
	}
}
