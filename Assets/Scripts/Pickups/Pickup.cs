﻿using UnityEngine;
using System.Collections;

public enum PickupType {
	Health,
	Points,
	Special
};

// Make sure there's an audio source
[RequireComponent(typeof(AudioSource))]
public class Pickup : MonoBehaviour {

	#region member vars
	protected PickupType type;
	bool picked = false;
	public SoundEvent tune;
	#endregion

	void OnTriggerEnter2D (Collider2D col) {
		// Only interact with the player
		if (col.tag != Tags.Player || picked) return;
		
		// get the player
		PlayerController pc = col.GetComponent<PlayerController>();
		PickupEffect(pc);
	}

	/// <summary>
	/// The effect of this pickup
	/// </summary>
	public virtual void PickupEffect(PlayerController pc) {
		picked = true;
		//might just want to gameobject.SetActive(false) here
		if (renderer) renderer.enabled = false;
		audio.PlayOneShot(audio.clip);
		pc.AddSound (tune);
		CleanUp ();
	}

	/// <summary>
	/// Cleans up this pickup instance.
	/// </summary>
	protected void CleanUp() {
		// for now, just destroy it
		if (!audio.clip)
			return;
		StartCoroutine(WaitToDestroy(audio.clip.length));
	}

	/// <summary>
	/// Waits to destroy.
	/// </summary>
	/// <returns>The to destroy.</returns>
	/// <param name="time">Time to wait.</param>
	IEnumerator WaitToDestroy(float time) {
		float destroyTime = Time.time + time;
		/*print ("Current time: " + Time.time);
		print ("Destroy time: " + destroyTime);*/

		// don't destroy it yet
		while (Time.time < destroyTime) {
			yield return null;
		}

		// clear to destroy
		Destroy(gameObject);
	}
}
