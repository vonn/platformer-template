﻿using UnityEngine;
using System.Collections;

public class CheckpointPickup : Pickup {

	void Awake () {
		type = PickupType.Special;
	}
	
	public override void PickupEffect (PlayerController pc)
	{
		pc.HitCheckpoint (transform);
		base.PickupEffect (pc);
		gameObject.SetActive (false);
	}
}