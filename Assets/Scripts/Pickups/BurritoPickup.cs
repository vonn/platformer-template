﻿using UnityEngine;
using System.Collections;

public class BurritoPickup : Pickup {

	void Awake() {
		type = PickupType.Points;
	}

	public override void PickupEffect (PlayerController pc)
	{
		pc.AddPoint ();
		base.PickupEffect (pc);
	}
}