using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

[Serializable ()]
public class SaveData : ISerializable {
	public bool foundGem1 = false;
	public float score = 42;
	public int levelReached = 3;
	
	public SaveData (SerializationInfo info, StreamingContext ctxt)
	{
		//Get the values from info and assign them to the appropriate properties
		foundGem1 = (bool)info.GetValue("foundGem1", typeof(bool));
		score = (float)info.GetValue("score", typeof(float));
		
		levelReached = (int)info.GetValue("levelReached", typeof(int));
	}

	public SaveData() {
		foundGem1 = false;
		score = 0;
		levelReached = 1;
	}

	//Serialization function.
	
	public void GetObjectData (SerializationInfo info, StreamingContext ctxt)
	{
		
		info.AddValue("foundGem1", (foundGem1));
		info.AddValue("score", score);
		info.AddValue("levelReached", levelReached);
	}

	public void Save () {
		
		SaveData data = new SaveData ();
		
		Stream stream = File.Open("MySavedGame.game", FileMode.Create);
		BinaryFormatter bformatter = new BinaryFormatter();
		bformatter.Binder = new VersionDeserializationBinder(); 
		Debug.Log ("Writing Information");
		bformatter.Serialize(stream, data);
		stream.Close();
	}
	
	public void Load () {
		
		SaveData data = new SaveData ();
		Stream stream = File.Open("MySavedGame.gamed", FileMode.Open);
		BinaryFormatter bformatter = new BinaryFormatter();
		bformatter.Binder = new VersionDeserializationBinder(); 
		Debug.Log ("Reading Data");
		data = (SaveData)bformatter.Deserialize(stream);
		stream.Close();
	}
}

public sealed class VersionDeserializationBinder : SerializationBinder 
{ 
	public override Type BindToType( string assemblyName, string typeName )
	{ 
		if ( !string.IsNullOrEmpty( assemblyName ) && !string.IsNullOrEmpty( typeName ) ) 
		{ 
			Type typeToDeserialize = null; 
			
			assemblyName = Assembly.GetExecutingAssembly().FullName; 
			
			// The following line of code returns the type. 
			typeToDeserialize = Type.GetType( String.Format( "{0}, {1}", typeName, assemblyName ) ); 
			
			return typeToDeserialize; 
		} 
		
		return null; 
	} 
}
