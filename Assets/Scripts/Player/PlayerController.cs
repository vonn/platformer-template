using UnityEngine;
using System.Collections;

/// <summary>
/// Controls various aspects of the player.
/// </summary>
public class PlayerController : Body2D {
	public enum PlayerStates {
		Discovered,
		Normal
	}
	#region Member Variables
	public float jumpForce = .25f;
	public bool groundedLastFrame;
	public bool jumpNextChance;
	public int maxJumps = 2;
	public int numJumps;
	public SoundEvent deathSound;
	public PlayerStates pState;
	public Transform checkpoint;
	public bool climbing;
	float climbSpeed = .1f;
	bool canClimb = true;
	float climbCD = .5f;
	float nextClimbTime;
	#endregion

	#region Reference Fields
	GameController game;
	Transform spriteChild;
	Animator spriteAnim;
	SoundPlayback _sp;
	#endregion

	#region Unity Methods
	void Start () {
		pState = PlayerStates.Normal;
		checkpoint = transform;	//respawn in starting position if checkpoint is never reached
		game = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ();
		spriteChild = transform.FindChild ("Sprite");
		spriteAnim = spriteChild.GetComponent<Animator> ();
		_sp = GetComponent<SoundPlayback> ();
	}
	
	void Update () {
		// update base first
		base.Update();

		if (!climbing && nextClimbTime < Time.time) {
			canClimb = true;
		}

		if (props.grounded) {
			canClimb = true;
			climbing = false;
			numJumps = maxJumps;
		}
		// update lastFrame bools
		groundedLastFrame = props.grounded;

		if (jumpNextChance && props.grounded) {
			Jump(jumpForce);
			jumpNextChance = false;
		}
	}

	//for enemies, zones
	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag == Tags.Enemy || collider.transform.tag == Tags.DeathZone) {
			Die ();
		}
	}
	#endregion

	#region Movement/Mechanics

	/// <summary>
	/// Tell the player to try to jump
	/// </summary>
	public bool Jump(float force) {
		// only jump if grounded or double jumping
		if (((props.grounded || climbing) && numJumps > 1 ) ||
		    (!props.grounded && numJumps > 0) ) {

			props.grounded = false;

			if (climbing) ClimbOff();

			numJumps--;

			// nerf the last jump
			if (numJumps == 0) {
				force *= .8f;
			}

			// and apply the force to the body
			props.vel.y = force;
			// this will remove it from a moving platform if it is on one
			RestoreOriginalParent();
			return true;
		}
		else {
			// maybe i can jump in a few frames
			if (props.vel.y < 0) {
				TryJumpNextChance();
			}
		}
		return false;
	}

	void TryJumpNextChance() {
		if (props.vel.y > 0) return;

		int numRays = 3;
		float offset = _col.size.x / (numRays - 1);
		Vector2 origin = _col.bounds.min;
		Vector2 dir = props.vel.normalized;
		//dir.x = 0;
		float checkDist = props.vel.magnitude * 5f;
		bool anyHit = false;
		
		// check each ray
		for (int i = 0; i < numRays; i++) {
			if (Physics2D.Raycast (origin, dir, checkDist, Layers2D.groundMask)) {
				Debug.DrawRay(origin, dir.normalized*checkDist, Color.magenta);
				anyHit = true;
				print ("bonk");
			}
			else {
				Debug.DrawRay(origin, dir.normalized*checkDist, Color.cyan);
			}
			origin.x += offset;
		}

		jumpNextChance = anyHit;
	}
	#endregion

	#region Respond to Input
	public void Move(float speed) {
		if (_gc.currentState != GameStates.Playing) return;

		if (!climbing) {
			MoveX (speed);
			spriteAnim.SetBool("Moving",speed != 0);
		}
		else {
			MoveX (speed/2);
			// TODO maybe an anim state for sideways climbing
			//spriteAnim.SetBool("Moving",speed != 0);
		}
		//for turning around
		if (speed < 0)
			spriteChild.localScale = new Vector3(Mathf.Abs(spriteChild.localScale.x) * -1, spriteChild.localScale.y, spriteChild.localScale.z);
		else if (speed > 0)
			spriteChild.localScale = new Vector3(Mathf.Abs(spriteChild.localScale.x), spriteChild.localScale.y, spriteChild.localScale.z);
	}

	public void Climb(float vInput) {
		if (!canClimb || !climbing) return;

		props.vel.y = vInput * climbSpeed;
	}
	public void ClimbOn() {
		if (!canClimb || climbing) return;

		props.vel.y = 0;
		props.ignoreGravity = true;
		//print ("enter ladder");
		props.grounded = false;
		climbing = true;
		// restore numjumps
		numJumps = maxJumps;
	}
	public void ClimbOff() {
		if (!climbing) return;

		//print ("exit ladder");
		props.ignoreGravity = false;
		climbing = false;
		nextClimbTime = Time.time + climbCD;
		canClimb = false;
	}
	public void ClimbZenith() {
		//print ("top of ladder");
		props.ignoreGravity = false;
		props.vel.y = 0;
		canClimb = false;
	}

	public void JumpButtonPressed(float vInput) {
		if (_gc.currentState != GameStates.Playing) return;

		// Fall through 1way platforms
		if (vInput < -Constants.epsilon &&
		    props.grounded &&
		    props.currentGroundType == GroundType.OneWay &&
		    props.vel.magnitude < Constants.epsilon) {

			FallThroughOneWayPlatform();
			return;
		}

		// try to jump
		if (Jump (jumpForce)) {
			//print ("jumped");
		}
		else {
			//TryJumpNextChance();
		}
	}

	public void PlaySong() {
		_sp.Play ();
	}
	#endregion

	void Die() {
		_sp.AddSound (deathSound);
		game.GameOver ();
	}

	public void Respawn() {
		pState = PlayerStates.Normal;
		transform.position = checkpoint.position;
		props.vel = Vector2.zero;
		props.grounded = false;
	}

	#region Accessors
	public bool IsDiscovered() {
		return pState == PlayerStates.Discovered;
	}
	#endregion

	public void HitCheckpoint(Transform t) {
		checkpoint = t;
		game.CheckpointReached ();
	}

	public void AddPoint() {
		game.AddPoint ();
	}

	public void AddSound(SoundEvent s) {
		_sp.AddSound (s);
	}
}