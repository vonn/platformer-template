﻿using UnityEngine;
using System.Collections;

/// <summary>
/// An interface between the user's input and the player controller
/// </summary>
public class PlayerInputController : MonoBehaviour {
	[SerializeField]
	PlayerController _pc;
	[SerializeField]
	float verticalInput;
	[SerializeField]
	float horizontalInput;

	// Use this for initialization
	void Start () {
		_pc = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		// update input variables
		verticalInput = Input.GetAxis("Vertical");
		horizontalInput = Input.GetAxis("Horizontal");

		// Horizontal movement
		_pc.Move (horizontalInput);

		// Jump button
		if (Input.GetButtonDown("Jump")) {
			_pc.JumpButtonPressed(verticalInput);
		}

		// Play a lil song
		if (Input.GetButtonDown("Vertical")) {
			_pc.PlaySong();
		}

		// Ladder
		if (_pc.climbing) {
			_pc.Climb(verticalInput);
		}
	}
}
