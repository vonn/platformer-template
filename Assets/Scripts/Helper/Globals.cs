/*
Various Global values and static classes.
 */

using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Layers and layermasks
/// </summary>
public class Layers2D {
	// built-in Unity layers
	const int defaultLayer = 0;
	const int transparentFXLayer = 1;
	const int waterLayer = 4;
	const int uiLayer = 5;
	// User-defined layers
	const int terrainLayer = 8;
	const int hazardLayer = 9;
	const int attackLayer = 10;
	const int enemyLayer = 11;

	// layer masks

	/// <summary>
	/// Things that harm the player
	/// </summary>
	public const int playerHarmMask = 1 << attackLayer |
									  1 << hazardLayer |
			                          1 << enemyLayer;

	public const int groundMask = 1 << terrainLayer;

	public const int defaultLayerMask = 1 << defaultLayer;
}

/// <summary>
/// Constants.
/// </summary>
public class Constants  {
	public const float epsilon = .01f;
	public static Vector2 crudVector = new Vector2(-666, -666);
}

/// <summary>
/// Tags.
/// </summary>
public class Tags { 
	public const string Player = "Player";
	public const string GameController = "GameController";
	public const string Enemy = "Enemy";
	public const string Checkpoint = "Checkpoint";
	public const string DeathZone = "DeathZone";
	public const string Terrain = "Terrain";
}

public class Scenes {
	public const string Menu = "Menu";
	public const string StageSelect = "StageSelect";
	//...
}

/// <summary>
/// Settings.
/// </summary>
public class Settings { 
	public static float volSFX = 1.0f;
	public static float volBGM = 1.0f;
}

/// <summary>
/// can't index array with enum so i have a class ok
/// </summary>
public class Bounds2D { 
	public const int TOP = 0;
	public const int BOTTOM = 1;
	public const int LEFT = 2;
	public const int RIGHT = 3;
}

